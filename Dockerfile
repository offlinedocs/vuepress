# Build
FROM node:latest AS builder

RUN git clone https://github.com/vuejs/vuepress.git /vuepress

WORKDIR /vuepress/packages/docs

RUN npm install
RUN npm run build

# Serve
FROM nginx:alpine

COPY --from=builder /vuepress/vuepress /usr/share/nginx/html
